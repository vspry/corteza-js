/* BEGIN_COPYRIGHT_HEADER

Copyright Vspry International Limited (c) 2020
All rights reserved.

END_COPYRIGHT_HEADER */

/** @type {import('jest').Config} */
const config = {
  verbose: true,
  transform: {
    "^.+\\.(t|j)sx?$": [
      "esbuild-jest",
      {
        sourcemap: true,
      }
    ]
  }
}
module.exports = config;