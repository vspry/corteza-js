"use strict";
var __defProp = Object.defineProperty;
var __getOwnPropDesc = Object.getOwnPropertyDescriptor;
var __getOwnPropNames = Object.getOwnPropertyNames;
var __hasOwnProp = Object.prototype.hasOwnProperty;
var __export = (target, all) => {
  for (var name in all)
    __defProp(target, name, { get: all[name], enumerable: true });
};
var __copyProps = (to, from, except, desc) => {
  if (from && typeof from === "object" || typeof from === "function") {
    for (let key of __getOwnPropNames(from))
      if (!__hasOwnProp.call(to, key) && key !== except)
        __defProp(to, key, { get: () => from[key], enumerable: !(desc = __getOwnPropDesc(from, key)) || desc.enumerable });
  }
  return to;
};
var __toCommonJS = (mod) => __copyProps(__defProp({}, "__esModule", { value: true }), mod);

// src/helpers.ts
var helpers_exports = {};
__export(helpers_exports, {
  createValueList: () => createValueList,
  formatCortezaDeleteResponse: () => formatCortezaDeleteResponse,
  formatCortezaRecord: () => formatCortezaRecord,
  formatCortezaResponse: () => formatCortezaResponse,
  formatPaginatedCortezaResponse: () => formatPaginatedCortezaResponse,
  isCortezaID: () => isCortezaID,
  updateValueList: () => updateValueList
});
module.exports = __toCommonJS(helpers_exports);
var import_util = require("util");
var formatValue = (v) => {
  if (typeof v === "boolean" || typeof v === "number")
    return v.toString();
  if (typeof v === "string")
    return v.replace("'", "\u2019");
  try {
    if (typeof v === "object") {
      if (v === null)
        return null;
      return JSON.stringify(v);
    }
    if (typeof v === "undefined")
      return null;
    return String(v);
  } catch (e) {
    return null;
  }
};
var formatCortezaValue = (v) => {
  if (v === "true" || v === "false")
    return v === "true";
  try {
    if (v.startsWith("{") || v.startsWith("[")) {
      const obj = JSON.parse(v);
      return obj;
    }
    return v;
  } catch (e) {
    return v;
  }
};
var createValueList = (object) => {
  const values = [];
  Object.keys(object).forEach((k) => {
    const val = object[k];
    if (Array.isArray(val)) {
      val.forEach((subVal) => {
        if (subVal === null)
          return;
        const v = formatValue(subVal);
        if (v === null)
          return;
        values.push({ name: k, value: v });
      });
    } else {
      const v = formatValue(val);
      if (v === null)
        return;
      values.push({ name: k, value: v });
    }
  });
  return values;
};
var valuesToObject = (values) => {
  const obj = {};
  values.forEach(({ name, value }) => {
    if (value === void 0)
      return;
    const existing = obj[name];
    const valFormatted = formatCortezaValue(value);
    if (Array.isArray(existing))
      existing.push(valFormatted);
    else if (existing)
      obj[name] = [existing, valFormatted];
    else
      obj[name] = valFormatted;
  });
  return obj;
};
var updateValueList = (original, updates) => {
  const existingObj = valuesToObject(original);
  const newObj = { ...existingObj, ...updates };
  return createValueList(newObj);
};
var formatCortezaRecord = (object) => {
  const { values } = object;
  if (!values || !Array.isArray(values))
    return object;
  const formatted = { ...object, ...valuesToObject(values) };
  return formatted;
};
var checkCortezaResponse = (res) => {
  if (!res)
    return null;
  const { error, response, success } = res;
  if (error || !response) {
    if (typeof response === "undefined")
      console.error(`Corteza Error: `, (0, import_util.inspect)(error, { depth: null, colors: true }));
    return null;
  }
  return { response, success };
};
var formatCortezaResponse = (res) => {
  const checkedResponse = checkCortezaResponse(res);
  if (!checkedResponse)
    return null;
  if (checkedResponse.response.set)
    return checkedResponse.response.set.map((r) => formatCortezaRecord(r));
  return formatCortezaRecord(checkedResponse.response);
};
var formatCortezaDeleteResponse = (res) => {
  const checkedResponse = checkCortezaResponse(res);
  if (!checkedResponse)
    return null;
  if (checkedResponse.success)
    return checkedResponse.success.message;
  return null;
};
var formatPaginatedCortezaResponse = (res) => {
  const checkedResponse = checkCortezaResponse(res);
  if (!checkedResponse)
    return null;
  const { response: { set, filter } } = checkedResponse;
  if (!set || !filter)
    return null;
  const k = { set: set.map((r) => formatCortezaRecord(r)), filter };
  return k;
};
var isCortezaID = (id) => {
  return id.match(/^\d{18}$/gm);
};
// Annotate the CommonJS export names for ESM import in node:
0 && (module.exports = {
  createValueList,
  formatCortezaDeleteResponse,
  formatCortezaRecord,
  formatCortezaResponse,
  formatPaginatedCortezaResponse,
  isCortezaID,
  updateValueList
});
