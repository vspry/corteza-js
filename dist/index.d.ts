import { RestQuery } from './rest';
import type { NamespaceID, ModuleID, PageID, RecordID, CortezaNamespace, CortezaModule, CortezaPage, CortezaRecord, CortezaAttachment, CortezaRole, CortezaPermission, RoleID, CortezaFieldOption, WorkflowID, CortezaWorkflowResult, CortezaFieldLabel, CortezaPaginatedResponse, CortezaTemplate, TemplateID, CortezaTemplateOptions } from './types';
interface CortezaQuery extends Omit<RestQuery, 'url'> {
    path: string;
}
export type CortezaFilter = {
    query?: string;
    deleted?: 0 | 1 | 2;
    limit?: number;
    pageCursor?: string;
    sort?: string;
};
type Credentials = {
    client_id: string;
    client_secret: string;
};
type AuthToken = {
    token: string;
    expiry: number;
};
type ClientOptions = {
    moduleMap?: Record<string, string>;
    templateMap?: Record<string, string>;
    trace?: boolean;
    namespace?: NamespaceID;
    timeout?: number;
    maxRetry?: number;
};
declare class CortezaClient {
    _baseurl: string;
    _namespacePath: string;
    _token: string;
    _b64Credentials: string;
    _authToken: AuthToken;
    _moduleMap: Record<string, string> | null;
    _templateMap: Record<string, string> | null;
    _trace: boolean;
    _timeout: number;
    _maxRetry: number;
    namespace: CortezaNamespace;
    constructor(baseurl: string, credentials: Credentials, options?: ClientOptions);
    static create(baseurl: string, namespace: string, credentials: Credentials, options?: ClientOptions): Promise<CortezaClient>;
    _buildNamespacePath(namespace: string): string;
    _requestToken(): Promise<{
        token: any;
        expiry: number;
    }>;
    _getToken(): Promise<any>;
    _getModules(): Promise<Record<string, string> | null>;
    _getTemplates(): Promise<Record<string, string> | null>;
    _checkModuleID(moduleID: ModuleID): Promise<ModuleID>;
    _checkTemplateID(templateID: TemplateID): Promise<TemplateID>;
    _getWorkflowID(workflowHandle: string): Promise<WorkflowID>;
    _checkWorkflowID(workflowID: WorkflowID): Promise<WorkflowID>;
    _getAll<A>(getter: (filter: {
        pageCursor: string | undefined;
        limit: 500;
    }) => Promise<CortezaPaginatedResponse<A> | null>): Promise<A[] | null>;
    raw({ method, path, data, params, headers, timeout, ...rest }: CortezaQuery): Promise<any>;
    query(queryConfig: CortezaQuery): Promise<Record<string, unknown> | Record<string, unknown>[] | null>;
    paginatedQuery<A = Record<string, unknown>>({ params: { pageCursor, ...restParams }, ...rest }: CortezaQuery & {
        params: NonNullable<CortezaQuery['params']> & {
            pageCursor?: string;
        };
    }): Promise<CortezaPaginatedResponse<A> | null>;
    getRoles(): Promise<CortezaRole[] | null>;
    getGenericPermissions(): Promise<CortezaPermission[] | null>;
    getRolePermissions(roleID: RoleID): Promise<CortezaPermission[] | null>;
    getNamespaces(): Promise<CortezaNamespace[]>;
    getNamespace(namespaceHandle: string): Promise<CortezaNamespace>;
    setNamespace(namespaceID: NamespaceID): Promise<Record<string, string> | null>;
    getModules(filter?: Omit<CortezaFilter, 'limit' | 'pageCursor'>): Promise<CortezaModule[] | null>;
    getPaginatedModules(filter?: CortezaFilter): Promise<CortezaPaginatedResponse<CortezaModule> | null>;
    getModule(moduleID: ModuleID): Promise<CortezaModule | null>;
    getModuleFieldOptions(moduleHandle: ModuleID, fieldName: string): Promise<CortezaFieldOption[] | null>;
    getModuleFieldLabels(moduleHandle: ModuleID): Promise<CortezaFieldLabel[] | null>;
    getPages(filter?: Omit<CortezaFilter, 'limit' | 'pageCursor'>): Promise<CortezaPage[] | null>;
    getPaginatedPages(filter?: CortezaFilter): Promise<CortezaPaginatedResponse<Record<string, unknown>> | null>;
    getPage(pageID: PageID): Promise<CortezaPage | null>;
    countRecords(moduleID: ModuleID, query?: string, timeout?: number): Promise<number | null>;
    getPaginatedRecords(moduleID: ModuleID, { limit, ...rest }: CortezaFilter, timeout?: number): Promise<CortezaPaginatedResponse<CortezaRecord> | null>;
    getRecords(moduleID: ModuleID, filter?: Omit<CortezaFilter, 'limit' | 'pageCursor'>, timeout?: number): Promise<CortezaRecord[] | null>;
    getRecord(moduleID: ModuleID, recordID: RecordID, timeout?: number): Promise<CortezaRecord | null>;
    getAttachment(attachmentID: RecordID, timeout?: number): Promise<CortezaAttachment | null>;
    getAttachmentData(attachmentID: RecordID, timeout?: number): Promise<unknown | null>;
    createRecord(moduleID: ModuleID, recordData: Partial<CortezaRecord>, timeout?: number): Promise<CortezaRecord | null>;
    createRecords(moduleID: ModuleID, recordDatas: Partial<CortezaRecord>[], timeout?: number): Promise<CortezaRecord[] | null>;
    createAttachment(moduleID: ModuleID, fieldName: string, attachmentName: string, attachmentBuffer: Buffer, timeout?: number): Promise<CortezaAttachment | null>;
    updateRecord(moduleID: ModuleID, recordID: RecordID, recordData: Partial<CortezaRecord>, timeout?: number): Promise<CortezaRecord | null>;
    deleteRecord(moduleID: ModuleID, recordID: RecordID): Promise<"OK" | null>;
    deleteRecords(moduleID: ModuleID, recordIDs: RecordID[]): Promise<"OK" | null>;
    deleteAllModuleRecords(moduleID: ModuleID): Promise<"OK" | null>;
    triggerWorkflow(workflow: WorkflowID, moduleID: ModuleID, recordID: RecordID, stepID: string, trace?: boolean, timeout?: number): Promise<CortezaWorkflowResult | null>;
    getTemplates(filter?: Omit<CortezaFilter, 'limit' | 'pageCursor'>): Promise<CortezaTemplate[] | null>;
    getPaginatedTemplates(filter?: CortezaFilter): Promise<CortezaPaginatedResponse<CortezaTemplate> | null>;
    getTemplate(templateID: TemplateID): Promise<CortezaTemplate | null>;
    renderTemplate(templateID: TemplateID, fileName: string, ext: 'html' | 'txt', data: NonNullable<object>, options?: never): Promise<string | null>;
    renderTemplate(templateID: TemplateID, fileName: string, ext: 'pdf', data: NonNullable<object>, options?: CortezaTemplateOptions): Promise<string | null>;
}
export { CortezaClient as default, CortezaClient };
export * from './types';
