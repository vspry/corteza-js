export type RoleID = string;
export type NamespaceID = string;
export type ModuleID = string;
export type FieldID = string;
export type RecordID = string;
export type PageID = string;
export type CortezaBoolean = 'true' | 'false' | boolean;
export type CortezaGeography = {
    coordinates: [lat: number, long: number];
};
export type CortezaPermissionAccess = 'allow' | 'inherit' | 'deny';
export type ApplicationID = string;
export type WorkflowID = string;
export type CortezaApplication = {
    applicationID: ApplicationID;
    name: string;
    ownerID: number;
    enabled: CortezaBoolean;
    unify: {
        name: string;
        listed: CortezaBoolean;
        url: string;
        config: string;
        icon: string;
        iconID: string;
        logo: string;
        logoID: string;
    };
};
export type CortezaRecordValue = {
    name: string;
    value: string;
};
export type CortezaRecord = {
    recordID: RecordID;
    moduleID: ModuleID;
    namespaceID: NamespaceID;
    createdAt: string;
    values: CortezaRecordValue[];
    [key: string]: unknown;
};
export type CortezaAttachment = {
    attachmentID: RecordID;
    ownerID: RecordID;
    url: string;
    previewUrl: string;
    name: string;
    meta: Record<string, unknown> | string;
    namespaceID: string;
};
export type CortezaNamespace = {
    slug: string;
    name: string;
    namespaceID: NamespaceID;
    handle: string;
};
export type CortezaRole = {
    roleID: RoleID;
    handle: string;
    name: string;
};
export type CortezaPermission = {
    access: CortezaPermissionAccess;
    operation: string;
    resource: string;
    roleID: RoleID;
};
export type CortezaFieldOption = {
    text: string;
    value: string;
};
export type CortezaFieldLabel = {
    name: string;
    label: string;
};
export type CortezaFieldOptions = {
    description: {
        view: string;
    };
    hint: {
        view: string;
    };
    multiDelimiter: string;
    options: CortezaFieldOption[];
    selectType: string;
};
export type CortezaModuleField = {
    createdAt: string;
    defaultValue: CortezaRecordValue[];
    fieldID: FieldID;
    kind: string;
    label: string;
    moduleID: ModuleID;
    name: string;
    namespaceID: NamespaceID;
    options: CortezaFieldOptions;
    updatedAt: string;
    isRequired: boolean;
    isMulti: boolean;
};
export type CortezaModule = {
    createdAt: string;
    fields: CortezaModuleField[];
    handle: string;
    meta: Record<string, unknown>;
    moduleID: ModuleID;
    name: string;
    namespaceID: NamespaceID;
    updatedAt: string;
};
export type CortezaPage = {
    namespaceID: NamespaceID;
    pageID: PageID;
    moduleID: ModuleID;
    handle: string;
    title: string;
    description: string;
    visible: CortezaBoolean;
    weight: number;
};
type CortezaPageNavigation = {
    page: number;
    items: number;
    cursor: string | null;
}[];
type CortezaResponseFilter = {
    moduleID?: ModuleID;
    namespaceID?: NamespaceID;
    query: string;
    deleted: number;
    sort: string;
    limit: number;
    prevPage?: string;
    nextPage?: string;
    pageNavigation?: CortezaPageNavigation;
    total?: number;
    [key: string]: unknown;
};
export type CortezaResponse = {
    response?: {
        set?: CortezaRecord[];
        filter?: CortezaResponseFilter;
        [key: string]: unknown;
    };
    success?: {
        message: string;
    };
    error?: {
        message: string;
    };
};
export type CortezaPaginatedResponse<A> = {
    set: A[];
    filter: CortezaResponseFilter;
};
type CortezaWorkflowFieldType = {
    '@type': string;
    '@value': Record<string, unknown>;
};
export type CortezaWorkflowResult = {
    results: {
        eventType: CortezaWorkflowFieldType;
        invoker: CortezaWorkflowFieldType;
        module: CortezaWorkflowFieldType;
        namespace: CortezaWorkflowFieldType;
        oldRecord: CortezaWorkflowFieldType;
        record: CortezaWorkflowFieldType;
        recordValueErrors: CortezaWorkflowFieldType;
        resourceType: CortezaWorkflowFieldType;
        runner: CortezaWorkflowFieldType;
        selected: CortezaWorkflowFieldType;
    };
};
export * from './templates';
