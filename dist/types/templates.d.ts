export type TemplateID = string;
export type CortezaTemplate = {
    templateID: TemplateID;
    handle: string;
    language: string;
    type: string;
    partial: boolean;
    meta: {
        short: string;
        description: string;
    };
    template: string;
    ownerID: string;
    createdAt: string;
    updatedAt: string;
    canGrant: boolean;
    canUpdateTemplate: boolean;
    canDeleteTemplate: boolean;
};
type CortezaTemplateDocumentSize = 'A0' | 'A1' | 'A2' | 'A3' | 'A4' | 'A5' | 'A6' | 'A7' | 'A8' | 'A9' | 'A10' | 'B0' | 'B1' | 'B2' | 'B3' | 'B4' | 'B5' | 'B6' | 'B7' | 'B8' | 'B9' | 'B10' | 'C0' | 'C1' | 'C2' | 'C3' | 'C4' | 'C5' | 'C6' | 'C7' | 'C8' | 'C9' | 'C10' | 'ANSI A' | 'ANSI B' | 'ANSI C' | 'ANSI D' | 'ANSI E' | 'junior legal' | 'letter' | 'legal' | 'tabloid';
export type CortezaTemplateOptions = {
    marginBottom?: string;
    marginLeft?: string;
    marginRight?: string;
    marginTop?: string;
    marginY?: string;
    marginX?: string;
    margin?: string;
    documentSize?: CortezaTemplateDocumentSize;
    documentWidth?: string;
    documentHeight?: string;
    contentScale?: string;
    orientation?: 'landscape' | 'portrait';
};
export {};
