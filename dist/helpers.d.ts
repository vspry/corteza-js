import type { CortezaResponse, CortezaRecord, CortezaRecordValue, CortezaPaginatedResponse } from './types';
export declare const createValueList: (object: Record<string, unknown>) => Array<CortezaRecordValue>;
export declare const updateValueList: (original: Array<CortezaRecordValue>, updates: Record<string, unknown>) => Array<CortezaRecordValue>;
export declare const formatCortezaRecord: (object: CortezaRecord) => Record<string, unknown>;
export declare const formatCortezaResponse: (res: CortezaResponse | null) => Record<string, unknown> | Record<string, unknown>[] | null;
export declare const formatCortezaDeleteResponse: (res: CortezaResponse | null) => string | null;
export declare const formatPaginatedCortezaResponse: <A>(res: CortezaResponse | null) => CortezaPaginatedResponse<A> | null;
export declare const isCortezaID: (id: string) => RegExpMatchArray | null;
