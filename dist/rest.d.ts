import { ResponseType, responseEncoding } from 'axios';
export type AxiosMethod = 'GET' | 'get' | 'POST' | 'post' | 'DELETE' | 'delete' | 'HEAD' | 'head' | 'OPTIONS' | 'options' | 'PUT' | 'put' | 'PATCH' | 'patch';
export interface RestQuery {
    method: AxiosMethod;
    url: string;
    headers?: Record<string, string | number | boolean>;
    data?: unknown;
    params?: Record<string, string | number | boolean | undefined>;
    debug?: boolean;
    maxBodyLength?: number;
    maxContentLength?: number;
    timeout?: number;
    maxRetry?: number;
    responseType?: ResponseType;
    responseEncoding?: responseEncoding;
}
export declare const restQuery: <R = any>(query: RestQuery, retry?: number) => Promise<R | null>;
