/* BEGIN_COPYRIGHT_HEADER

Copyright Vspry International Limited (c) 2020
All rights reserved.

END_COPYRIGHT_HEADER */
import { inspect } from 'util'

import type { CortezaResponse, CortezaRecord, CortezaRecordValue, CortezaPaginatedResponse } from './types'

// converts a value into a corteza appropriate value
const formatValue = (v: unknown): string | null => {
    if (typeof v === 'boolean' || typeof v === 'number') return v.toString()
    if (typeof v === 'string') return v.replace("'", "’")
    try {
        if (typeof v === 'object') {
            if (v === null) return null
            return JSON.stringify(v)
        }
        if (typeof v === 'undefined') return null
        return String(v)
    } catch (e) {
        return null
    }
}

// converts a corteza value back to a standard value
const formatCortezaValue = (v: string) => {
    if (v === 'true' || v === 'false') return v === 'true'
    try {
        if (v.startsWith('{') || v.startsWith('[')) {
            const obj = JSON.parse(v)
            return obj
        }
        return v
    } catch(e) {
        return v
    }
}

// creates a value list from an object
export const createValueList = (object: Record<string, unknown>): Array<CortezaRecordValue> => {
    const values: CortezaRecordValue[] = []
    Object.keys(object).forEach((k) => {
        const val = object[k]
        // if array, each value of the array is its own value list item with the same name
        if (Array.isArray(val)) {
            val.forEach((subVal) => {
                if (subVal === null) return // null values prevent the whole list from displaying in app for some reason
                const v = formatValue(subVal)
                if (v === null) return
                values.push({ name: k, value: v })
            })
        } else {
            const v = formatValue(val)
            if (v === null) return
            values.push({ name: k, value: v })
        }
    })
    return values
}

// converts a list of values pairs to an object
const valuesToObject = (values: Array<CortezaRecordValue>): Record<string, unknown> => {
    const obj: Record<string, unknown> = {}
    values.forEach(({ name, value }) => {
        if (value === undefined) return // omitting a no value field with make it empty in corteza
        const existing = obj[name]
        const valFormatted = formatCortezaValue(value)
        // values with the same name become an array of values
        if (Array.isArray(existing)) existing.push(valFormatted)
        else if (existing) obj[name] = [existing, valFormatted]
        else obj[name] = valFormatted
    })

    return obj
}

// adds the contents of an object to a value list
export const updateValueList = (original: Array<CortezaRecordValue>, updates: Record<string, unknown>): Array<CortezaRecordValue> => {
    const existingObj = valuesToObject(original)
    const newObj: Record<string, unknown> = { ...existingObj, ...updates }

    return createValueList(newObj)
}


// function that finds any values [] arrays in an object and moves the values out to the top level of the object
// also adds an id field
// i.e. { id: 10, values: [{ name: "name", value: "john smith" }] } => { id: 10, name: "john smith" }
export const formatCortezaRecord = (object: CortezaRecord): Record<string, unknown> => {
    const { values } = object
    if (!values || !Array.isArray(values)) return object

    const formatted = { ...object, ...valuesToObject(values) }
    return formatted
}

const checkCortezaResponse = (res: CortezaResponse | null) => {
    if (!res) return null
    const { error, response, success } = res

    if (error || !response) {
        if (typeof response === 'undefined') console.error(`Corteza Error: `, inspect(error, { depth: null, colors: true }))
        return null
    }
    return { response, success }
}

// formats a corteza response to be consistent with old crm system implementation (Zoho)
export const formatCortezaResponse = (res: CortezaResponse | null) => {
    const checkedResponse = checkCortezaResponse(res)
    if (!checkedResponse) return null

    if (checkedResponse.response.set) return checkedResponse.response.set.map((r) => formatCortezaRecord(r))
    return formatCortezaRecord(checkedResponse.response as CortezaRecord)
}

export const formatCortezaDeleteResponse = (res: CortezaResponse | null) => {
    const checkedResponse = checkCortezaResponse(res)
    if (!checkedResponse) return null

    if (checkedResponse.success) return checkedResponse.success.message
    return null
}

export const formatPaginatedCortezaResponse = <A>(res: CortezaResponse | null) => {
    const checkedResponse  = checkCortezaResponse(res)
    if (!checkedResponse) return null

    const { response: { set, filter } } = checkedResponse
    if (!set || !filter) return null

    const k = { set: set.map((r) => formatCortezaRecord(r)), filter }

    return k as CortezaPaginatedResponse<A>
}

// checks if a string matches the format of a corteza system ID
export const isCortezaID = (id: string) => {
    return id.match(/^\d{18}$/gm)
}
