/* BEGIN_COPYRIGHT_HEADER

Copyright Vspry International Limited (c) 2020
All rights reserved.

END_COPYRIGHT_HEADER */

import FormData from 'form-data'

import {
    isCortezaID,
    createValueList,
    formatCortezaResponse,
    updateValueList,
    formatCortezaDeleteResponse,
    formatPaginatedCortezaResponse,
    formatCortezaRecord,
} from './helpers'
import { restQuery, RestQuery } from './rest'
import type {
    NamespaceID,
    ModuleID,
    PageID,
    RecordID,
    CortezaNamespace,
    CortezaModule,
    CortezaPage,
    CortezaRecord,
    CortezaAttachment,
    CortezaRole,
    CortezaPermission,
    RoleID,
    CortezaFieldOption,
    WorkflowID,
    CortezaWorkflowResult,
    CortezaFieldLabel,
    CortezaPaginatedResponse,
    CortezaTemplate,
    TemplateID,
    CortezaTemplateOptions,
    CortezaResponse,
} from './types'

interface CortezaQuery extends Omit<RestQuery, 'url'> {
    path: string
}

export type CortezaFilter = {
    query?: string
    deleted?: 0 | 1 | 2 // Exclude (0, default), include (1) or return only (2) deleted records
    limit?: number
    pageCursor?: string
    sort?: string
}

type Credentials = {
    client_id: string
    client_secret: string
}

type AuthToken = {
    token: string
    expiry: number
}

type ClientOptions = {
    moduleMap?: Record<string, string>
    templateMap?: Record<string, string>
    trace?: boolean
    namespace?: NamespaceID
    timeout?: number
    maxRetry?: number
}

class CortezaClient {
    // private members
    _baseurl: string
    _namespacePath!: string
    _token!: string
    _b64Credentials: string
    _authToken!: AuthToken
    _moduleMap: Record<string, string> | null
    _templateMap: Record<string, string> | null
    _trace: boolean
    _timeout: number
    _maxRetry: number

    // public members
    namespace!: CortezaNamespace

    constructor(baseurl: string, credentials: Credentials, options?: ClientOptions) {
        this._baseurl = baseurl
        this._b64Credentials = Buffer.from(`${credentials.client_id}:${credentials.client_secret}`).toString('base64')
        this._moduleMap = options?.moduleMap ?? null
        this._templateMap = options?.templateMap ?? null
        this._trace = options?.trace ? options.trace : false
        this._timeout = options?.timeout ?? 20000
        this._maxRetry = options?.maxRetry ?? 3
        if (options?.namespace) this.setNamespace(options?.namespace)
    }

    static async create(baseurl: string, namespace: string, credentials: Credentials, options?: ClientOptions) {
        const client = new CortezaClient(baseurl, credentials, options)
        await client.setNamespace(namespace)

        return client
    }

    // eslint-disable-next-line class-methods-use-this
    _buildNamespacePath(namespace: string) {
        return `api/compose/namespace/${namespace}`
    }

    async _requestToken() {
        const res = await restQuery({
            method: 'POST',
            url: `${this._baseurl}/auth/oauth2/token`,
            data: `grant_type=client_credentials&scope=api`,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                Authorization: `Basic ${this._b64Credentials}`,
            },
            debug: this._trace,
            maxRetry: this._maxRetry,
        })

        if (!res) throw new Error('failed to retrieve corteza auth token.')

        const authToken = {
            token: res.access_token,
            expiry: new Date().getTime() + res.expires_in * 1000,
        }
        this._authToken = authToken
        return authToken
    }

    async _getToken() {
        if (this._authToken && this._authToken.token && this._authToken.expiry > Date.now()) return this._authToken.token
        const authToken = await this._requestToken()
        return authToken.token
    }

    async _getModules(): Promise<Record<string, string> | null> {
        const res = await this.getModules()
        if (!res || !Array.isArray(res)) return null
        const moduleMap = res.reduce((t, c) => ({ ...t, [c.handle.toLowerCase()]: c.moduleID }), {})
        this._moduleMap = moduleMap
        return moduleMap
    }

    async _getTemplates(): Promise<Record<string, string> | null> {
        const res = await this.getTemplates()
        if (!res || !Array.isArray(res)) return null
        const templateMap = res.reduce((t, c) => ({ ...t, [c.handle.toLowerCase()]: c.templateID }), {})
        this._templateMap = templateMap
        return templateMap
    }

    async _checkModuleID(moduleID: ModuleID): Promise<ModuleID> {
        if (isCortezaID(moduleID)) return moduleID

        if (!this._moduleMap) await this._getModules()
        if (this._moduleMap && this._moduleMap[moduleID]) return this._moduleMap[moduleID]
        throw new Error(`Module ${moduleID} could not be matched to any module ID`)
    }

    async _checkTemplateID(templateID: TemplateID): Promise<TemplateID> {
        if (isCortezaID(templateID)) return templateID

        if (!this._templateMap) await this._getTemplates()
        if (this._templateMap && this._templateMap[templateID]) return this._templateMap[templateID]
        throw new Error(`Template ${templateID} could not be matched to any template ID`)
    }

    async _getWorkflowID(workflowHandle: string): Promise<WorkflowID> {
        const workflows = await this.query({ method: 'GET', path: 'api/automation/workflows/' })
        if (!workflows || !Array.isArray(workflows) || !workflows.length) throw new Error('No workflows found')
        const workflow = workflows.find((w) => w.handle === workflowHandle)
        if (!workflow) throw new Error(`Cannot find workflow with handle ${workflowHandle}`)
        return workflow.workflowID as WorkflowID
    }

    async _checkWorkflowID(workflowID: WorkflowID): Promise<WorkflowID> {
        if (isCortezaID(workflowID)) return workflowID
        return this._getWorkflowID(workflowID)
    }

    // eslint-disable-next-line class-methods-use-this
    async _getAll<A>(
        getter: (filter: { pageCursor: string | undefined; limit: 500 }) => Promise<CortezaPaginatedResponse<A> | null>,
    ): Promise<A[] | null> {
        let nextPage
        const results: A[] = []
        do {
            const r = await getter({ pageCursor: nextPage, limit: 500 })
            if (!r) return null
            nextPage = r.filter.nextPage
            results.push(...r.set)
        } while (nextPage)

        return results
    }

    // for returning raw output of corteza api
    async raw({ method, path, data, params, headers, timeout, ...rest }: CortezaQuery) {
        const token = await this._getToken()
        const u = `${this._baseurl}/${path}`
        const h = {
            Authorization: `Bearer ${token}`,
            'Content-Type': 'application/json',
            ...headers,
        }

        return restQuery({
            method,
            url: u,
            headers: h,
            params,
            data,
            debug: this._trace,
            timeout: timeout ?? this._timeout,
            maxRetry: this._maxRetry,
            ...rest,
        })
    }

    // for returning output formatted for ease of use
    async query(queryConfig: CortezaQuery) {
        const res = await this.raw(queryConfig)
        return formatCortezaResponse(res)
    }

    async paginatedQuery<A = Record<string, unknown>>({
        params: { pageCursor, ...restParams },
        ...rest
    }: CortezaQuery & {
        params: NonNullable<CortezaQuery['params']> & { pageCursor?: string }
    }): Promise<CortezaPaginatedResponse<A> | null> {
        const res = await this.raw({
            ...rest,
            params: { ...restParams, pageCursor, incTotal: !pageCursor, incPageNavigation: !pageCursor },
        })
        return formatPaginatedCortezaResponse<A>(res)
    }

    async getRoles(): Promise<CortezaRole[] | null> {
        return this.query({ method: 'GET', path: `api/system/roles/` }) as Promise<CortezaRole[] | null>
    }

    async getGenericPermissions(): Promise<CortezaPermission[] | null> {
        return this.query({ method: 'GET', path: `/api/compose/permissions/` }) as Promise<CortezaPermission[] | null>
    }

    async getRolePermissions(roleID: RoleID): Promise<CortezaPermission[] | null> {
        return this.query({ method: 'GET', path: `api/compose/permissions/${roleID}/rules` }) as Promise<CortezaPermission[] | null>
    }

    async getNamespaces(): Promise<CortezaNamespace[]> {
        const namespaces = (await this.query({ method: 'GET', path: 'api/compose/namespace/' })) as CortezaNamespace[]
        if (!namespaces || !Array.isArray(namespaces) || !namespaces.length) throw new Error(`Unable to find Namespaces`)

        return namespaces
    }

    async getNamespace(namespaceHandle: string): Promise<CortezaNamespace> {
        const namespaces = await this.getNamespaces()

        const namespace = namespaces.find(
            (n) => n.slug.toLowerCase() === namespaceHandle.toLowerCase() || n.name.toLowerCase() === namespaceHandle.toLowerCase(),
        )
        if (!namespace) throw new Error(`Unable to find Namespace ${namespaceHandle}`)

        return namespace
    }

    async setNamespace(namespaceID: NamespaceID) {
        // reusables
        const _update = (nid: NamespaceID) => {
            this._namespacePath = this._buildNamespacePath(nid)
            return this._getModules()
        }

        // if input is id just update
        if (isCortezaID(namespaceID)) return _update(namespaceID)

        // if not id lookup and update
        const namespace = await this.getNamespace(namespaceID)

        this.namespace = namespace
        return _update(namespace.namespaceID)
    }

    async getModules(filter: Omit<CortezaFilter, 'limit' | 'pageCursor'> = {}) {
        return this._getAll<CortezaModule>((f) =>
            this.paginatedQuery({ method: 'GET', path: `${this._namespacePath}/module/`, params: { ...f, ...filter } }),
        )
    }

    async getPaginatedModules(filter: CortezaFilter = {}) {
        return this.paginatedQuery<CortezaModule>({ method: 'GET', path: `${this._namespacePath}/module/`, params: filter })
    }

    async getModule(moduleID: ModuleID): Promise<CortezaModule | null> {
        return this.query({ method: 'GET', path: `${this._namespacePath}/module/${moduleID}` }) as Promise<CortezaModule | null>
    }

    async getModuleFieldOptions(moduleHandle: ModuleID, fieldName: string): Promise<CortezaFieldOption[] | null> {
        const modules = await this.getModules()
        if (!modules) return null
        const module = modules.find((m) => m.handle === moduleHandle)
        if (!module) return null
        const field = module.fields.find((f) => f.name === fieldName)
        if (!field) return null
        return field.options.options
    }

    async getModuleFieldLabels(moduleHandle: ModuleID): Promise<CortezaFieldLabel[] | null> {
        const modules = await this.getModules()
        if (!modules) return null
        const module = modules.find((m) => m.handle === moduleHandle)
        if (!module) return null
        return module.fields.map((f) => ({ name: f.name, label: f.label }))
    }

    async getPages(filter: Omit<CortezaFilter, 'limit' | 'pageCursor'> = {}) {
        return this._getAll<CortezaPage>((f) => this.paginatedQuery({ method: 'GET', path: `${this._namespacePath}/page/`, params: { ...f, ...filter } }))
    }

    async getPaginatedPages(filter: CortezaFilter = {}) {
        return this.paginatedQuery({ method: 'GET', path: `${this._namespacePath}/page/`, params: filter })
    }

    async getPage(pageID: PageID): Promise<CortezaPage | null> {
        return this.query({ method: 'GET', path: `${this._namespacePath}/page/${pageID}` }) as Promise<CortezaPage | null>
    }

    async countRecords(moduleID: ModuleID, query?: string, timeout?: number): Promise<number | null> {
        const id = await this._checkModuleID(moduleID)
        const result = (await this.raw({
            method: 'GET',
            path: `${this._namespacePath}/module/${id}/record/`,
            params: query ? { query, incTotal: true, limit: 1 } : { incTotal: true, limit: 1 },
            timeout,
        })) as CortezaResponse
        if (!result.response?.filter) return null
        return result.response.filter.total ?? 0
    }

    async getPaginatedRecords(moduleID: ModuleID, { limit = 10, ...rest }: CortezaFilter, timeout?: number) {
        const id = await this._checkModuleID(moduleID)
        return this.paginatedQuery<CortezaRecord>({
            method: 'GET',
            path: `${this._namespacePath}/module/${id}/record/`,
            params: { limit, ...rest },
            timeout,
        })
    }

    async getRecords(moduleID: ModuleID, filter: Omit<CortezaFilter, 'limit' | 'pageCursor'> = {}, timeout?: number): Promise<CortezaRecord[] | null> {
        const id = await this._checkModuleID(moduleID)
        return this._getAll<CortezaRecord>((f) => this.paginatedQuery({
            method: 'GET',
            path: `${this._namespacePath}/module/${id}/record/`,
            params: { ...f, ...filter },
            timeout,
        }))
    }

    async getRecord(moduleID: ModuleID, recordID: RecordID, timeout?: number): Promise<CortezaRecord | null> {
        const id = await this._checkModuleID(moduleID)
        return this.query({
            method: 'GET',
            path: `${this._namespacePath}/module/${id}/record/${recordID}`,
            timeout,
        }) as Promise<CortezaRecord | null>
    }

    async getAttachment(attachmentID: RecordID, timeout?: number): Promise<CortezaAttachment | null> {
        return this.query({
            method: 'GET',
            path: `${this._namespacePath}/attachment/record/${attachmentID}`,
            timeout,
        }) as Promise<CortezaAttachment | null>
    }

    async getAttachmentData(attachmentID: RecordID, timeout?: number): Promise<unknown | null> {
        const attachment = await this.getAttachment(attachmentID, timeout)
        if (!attachment) return null

        return this.raw({
            method: 'GET',
            path: `api/compose${attachment.url}`,
            timeout,
        }) as Promise<unknown | null>
    }

    async createRecord(moduleID: ModuleID, recordData: Partial<CortezaRecord>, timeout?: number): Promise<CortezaRecord | null> {
        const id = await this._checkModuleID(moduleID)
        const values = createValueList(recordData)
        return this.query({
            method: 'POST',
            path: `${this._namespacePath}/module/${id}/record/`,
            data: { values },
            timeout,
        }) as Promise<CortezaRecord | null>
    }

    async createRecords(moduleID: ModuleID, recordDatas: Partial<CortezaRecord>[], timeout?: number): Promise<CortezaRecord[] | null> {
        const id = await this._checkModuleID(moduleID)
        const valuess = recordDatas.map(createValueList)
        const result = await this.query({
            method: 'POST',
            path: `${this._namespacePath}/module/${id}/record/`,
            data: { records: [{ set: valuess.map((v) => ({ values: v })) }] },
            timeout,
        })
        if (Array.isArray(result)) return result as CortezaRecord[]
        if (result === null) return null
        const { records, ...record } = result as CortezaRecord & { records: CortezaRecord[] }
        return [...records?.map(formatCortezaRecord) ?? [], record] as CortezaRecord[]
    }

    async createAttachment(
        moduleID: ModuleID,
        fieldName: string,
        attachmentName: string,
        attachmentBuffer: Buffer,
        timeout?: number,
    ): Promise<CortezaAttachment | null> {
        const id = await this._checkModuleID(moduleID)
        const formData = new FormData()
        formData.append('fieldName', fieldName)
        formData.append('upload', attachmentBuffer, attachmentName)

        return this.query({
            method: 'POST',
            path: `${this._namespacePath}/module/${id}/record/attachment`,
            headers: formData.getHeaders(),
            data: formData,
            maxBodyLength: Infinity,
            maxContentLength: Infinity,
            timeout,
        }) as Promise<CortezaAttachment | null>
    }

    async updateRecord(
        moduleID: ModuleID,
        recordID: RecordID,
        recordData: Partial<CortezaRecord>,
        timeout?: number,
    ): Promise<CortezaRecord | null> {
        const id = await this._checkModuleID(moduleID)
        const original = await this.getRecord(moduleID, recordID)
        if (!original) return null
        const values = updateValueList(original.values, recordData)

        return this.query({
            method: 'POST',
            path: `${this._namespacePath}/module/${id}/record/${recordID}`,
            data: { values },
            timeout,
        }) as Promise<CortezaRecord | null>
    }

    async deleteRecord(moduleID: ModuleID, recordID: RecordID) {
        const id = await this._checkModuleID(moduleID)
        const res = await this.raw({
            method: 'DELETE',
            path: `${this._namespacePath}/module/${id}/record/${recordID}`,
        })
        return formatCortezaDeleteResponse(res) as 'OK' | null
    }

    async deleteRecords(moduleID: ModuleID, recordIDs: RecordID[]) {
        const id = await this._checkModuleID(moduleID)
        const res = await this.raw({
            method: 'DELETE',
            path: `${this._namespacePath}/module/${id}/record/`,
            data: {
                recordIDs,
            },
        })
        return formatCortezaDeleteResponse(res) as 'OK' | null
    }

    async deleteAllModuleRecords(moduleID: ModuleID) {
        const id = await this._checkModuleID(moduleID)
        const records = await this.getRecords(id)
        if (!records || !records.length) return 'OK'
        return this.deleteRecords(
            id,
            records.map((r) => r.recordID),
        )
    }

    async triggerWorkflow(
        workflow: WorkflowID,
        moduleID: ModuleID,
        recordID: RecordID,
        stepID: string,
        trace = false,
        timeout?: number,
    ): Promise<CortezaWorkflowResult | null> {
        const workflowID = await this._checkWorkflowID(workflow)

        if (!this.namespace) throw new Error('No namespace set')
        const checkedModuleID = await this._checkModuleID(moduleID)
        const module = await this.getModule(checkedModuleID)
        if (!module) throw new Error('Cannot find module to run workflow with')
        const record = await this.getRecord(checkedModuleID, recordID)
        if (!record) throw new Error('Cannot find record to run workflow with')

        return this.query({
            method: 'POST',
            path: `api/automation/workflows/${workflowID}/exec`,
            data: {
                async: true,
                input: {
                    module: {
                        '@type': 'ComposeModule',
                        '@value': module,
                    },
                    namespace: {
                        '@type': 'ComposeNamespace',
                        '@value': this.namespace,
                    },
                    oldRecord: {
                        '@type': 'ComposeRecord',
                        '@value': record,
                    },
                    record: {
                        '@type': 'ComposeRecord',
                        '@value': record,
                    },
                },
                stepID,
                trace,
                wait: false,
            },
            timeout,
        }) as unknown as CortezaWorkflowResult | null
    }

    async getTemplates(filter: Omit<CortezaFilter, 'limit' | 'pageCursor'> = {}) {
        return this._getAll<CortezaTemplate>((f) => this.paginatedQuery({ method: 'GET', path: `api/system/template/`, params: { ...f, ...filter } }))
    }

    async getPaginatedTemplates(filter: CortezaFilter = {}) {
        return this.paginatedQuery<CortezaTemplate>({ method: 'GET', path: `api/system/template/`, params: filter })
    }

    async getTemplate(templateID: TemplateID): Promise<CortezaTemplate | null> {
        const id = await this._checkTemplateID(templateID)
        return this.query({ method: 'GET', path: `api/system/template/${id}` }) as Promise<CortezaTemplate | null>
    }

    async renderTemplate(
        templateID: TemplateID,
        fileName: string,
        ext: 'html' | 'txt',
        data: NonNullable<object>,
        options?: never,
    ): Promise<string | null>
    async renderTemplate(
        templateID: TemplateID,
        fileName: string,
        ext: 'pdf',
        data: NonNullable<object>,
        options?: CortezaTemplateOptions,
    ): Promise<string | null>
    async renderTemplate(
        templateID: TemplateID,
        fileName: string,
        ext: 'pdf' | 'html' | 'txt',
        data: NonNullable<object>,
        options?: CortezaTemplateOptions,
    ): Promise<string | null> {
        const id = await this._checkTemplateID(templateID)
        return this.raw({
            method: 'POST',
            path: `api/system/template/${id}/render/${fileName}.${ext}`,
            data: { variables: data, options: options ?? {} },
            ...(ext === 'pdf' ? { responseType: 'arraybuffer', responseEncoding: 'binary' } : {}),
        })
    }
}
export { CortezaClient as default, CortezaClient }
export * from './types'
