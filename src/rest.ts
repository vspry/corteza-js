/* BEGIN_COPYRIGHT_HEADER

Copyright Vspry International Limited (c) 2020
All rights reserved.

END_COPYRIGHT_HEADER */

import axios, { AxiosError, ResponseType, responseEncoding } from 'axios'
import { inspect } from 'util'

export type AxiosMethod =
    | 'GET'
    | 'get'
    | 'POST'
    | 'post'
    | 'DELETE'
    | 'delete'
    | 'HEAD'
    | 'head'
    | 'OPTIONS'
    | 'options'
    | 'PUT'
    | 'put'
    | 'PATCH'
    | 'patch'

export interface RestQuery {
    method: AxiosMethod
    url: string
    headers?: Record<string, string | number | boolean>
    data?: unknown
    params?: Record<string, string | number | boolean | undefined>
    debug?: boolean
    maxBodyLength?: number,
    maxContentLength?: number,
    timeout?: number
    maxRetry?: number
    responseType?: ResponseType
    responseEncoding?: responseEncoding
}

// generic query function, will be used to normalize the method and return structure of all api calls
export const restQuery = async <R = any>(query: RestQuery, retry = 0): Promise<R | null> => {
    const { method, url, headers, params, data, debug = false, maxRetry, ...rest } = query
    const startTime = Date.now()
    try {
        const res = await axios({
            method,
            url,
            headers,
            params,
            data,
            timeout: 20000,
            ...rest
        })

        if (debug) console.info(`Response received from ${method} ${url}. \nstatus: ${res.status} latency: ${Date.now() - startTime}ms`)
        if (debug) console.info(inspect(res, false, 2))
        return res.data
    } catch (error) {
        if (error instanceof AxiosError && error.response?.status === 429 && retry < (maxRetry ?? 3)) {
            const retryTimeout = 500 * (retry + 1)
            if (debug) console.info(`Received 429 from ${url}. Retrying in ${retryTimeout}ms`)
            await new Promise((r) => setTimeout(r, retryTimeout))
            return restQuery(query, retry + 1)
        }
        console.error(`Error from ${url}. \nlatency: ${Date.now() - startTime}ms`)
        // await writeFile('error.json', JSON.stringify(error))

        if (error instanceof Error) {
            const { message, stack } = error
            const formatted = {
                method,
                url,
                headers: { request: headers, response: error instanceof AxiosError ? error.response?.headers : null },
                params,
                data,
                error: {
                    code: error instanceof AxiosError ? error.code : null,
                    message,
                    status: error instanceof AxiosError ? error.response?.status ?? error.status : null,
                    statusText: error instanceof AxiosError ? error.response?.statusText : null,
                    response: error instanceof AxiosError && debug ? error.response?.data : null,
                },
            }

            console.error(
                inspect(formatted, {
                    showHidden: false,
                    depth: null,
                    colors: true,
                })
            )
            if (debug) console.error(inspect(error, false, 2))
            if (debug) console.error(stack)
            return null
        }

        console.error('unknown error occurred:', error)
        return null
    }
}
