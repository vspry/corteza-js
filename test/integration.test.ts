/* BEGIN_COPYRIGHT_HEADER

Copyright Vspry International Limited (c) 2020
All rights reserved.

END_COPYRIGHT_HEADER */

import fs from 'fs'
import CortezaClient, { CortezaFilter, CortezaRecord } from '../src'
import 'dotenv/config'

export const CrmClient = new CortezaClient(process.env.CORTEZA_API_URL ?? '', {
    client_id: process.env.CORTEZA_CLIENT_ID ?? '',
    client_secret: process.env.CORTEZA_CLIENT_SECRET ?? '',
})

beforeAll(async () => {
    await CrmClient.setNamespace('pluto')
})

jest.setTimeout(30000000)

const data = [
    { name: 'Account_Name', value: 'KREDIT HERO' },
    { name: 'Account_Status', value: '1' },
    { name: 'Account_Assessed_ML_TF_Risk', value: 'Not_Assessed' },
    { name: 'Account_Biz_Tax_Id', value: '3423423' },
    { name: 'Account_Address_City', value: 'Taguig' },
    { name: 'Account_Address_Country', value: '291503399885602819' },
    {
        name: 'Account_ECDD_Review_Date',
        value: '2123-09-24T00:00:00Z',
    },
    { name: 'Account_Email', value: 'janna@kredithero.com' },
    { name: 'Account_Employer_Category', value: '9' },
    { name: 'Account_EntityType', value: '314868593961533697' },
    { name: 'Account_Entity_Type', value: 'Cooperative' },
    {
        name: 'Account_Firebase_Id',
        value: 'd899ff88-7c06-46eb-b16d-1c4f48e9fdaf',
    },
    {
        name: 'Account_FullName_Email',
        value: 'KREDIT HERO (janna@kredithero.com)',
    },
    { name: 'Account_Employ_Headcount', value: '0' },
    { name: 'Account_Identity', value: 'Unverified' },
    { name: 'Account_Industry', value: '338820141636780289' },
    {
        name: 'Account_IndustryDisplay',
        value: 'Advertising and Market Research',
    },
    { name: 'Account_Industry_Risk', value: 'Low' },
    {
        name: 'Account_Date_ID_Verified',
        value: '2024-09-24T00:00:00Z',
    },
    { name: 'Accounts_LoansSettled', value: '0' },
    { name: 'Account_ML_TF_Risk', value: 'Not_Assessed' },
    { name: 'Number', value: 'pAcG1MQ24qhk6268aW' },
    { name: 'NumberRaw', value: 'pAcG1MQ24qhk6268aW' },
    { name: 'Account_Owner_DOB', value: '1995-09-21T00:00:00Z' },
    {
        name: 'Account_Owner_HighestEducationLevel',
        value: 'Not specified',
    },
    { name: 'Account_Owner_Gender', value: 'Not specified' },
    { name: 'Account_Owner_MaritalStatus', value: 'Not specified' },
    { name: 'Account_OccupationGroup', value: 'Not specified' },
    { name: 'Account_Regulatory_Age', value: '28' },
    { name: 'Account_Phone', value: '+639270757420' },
    { name: 'Account_Address_Postcode', value: '1634' },
    { name: 'Account_Address_Province', value: 'Metro Manila' },
    { name: 'Account_Address_Street', value: '28th St' },
    { name: 'Account_Address_Suburb', value: 'Taguig' },
    { name: 'Account_Trading_Name', value: 'Kredit Hero' },
    { name: 'Account_Veto_Count', value: '0' },
    { name: 'Account_Segments', value: '347474519679566081' },
    { name: 'Account_Tags', value: '347492789765800193' },
    { name: 'Account_Tags', value: '347492713211363585' },
    { name: 'Account_BizFlag', value: '1' },
    { name: 'AuthorisedUsers', value: '291503379081920515' },
    { name: 'AuthorisedUsers', value: '291503379098632195' },
    { name: 'AuthorisedUsers', value: '291503379115409411' },
    { name: 'AuthorisedUsers', value: '327029406121853185' },
    { name: 'AuthorisedUsers', value: '291503379065208835' },
    { name: 'AuthorisedUsers', value: '390802094296275770' },
    { name: 'AuthorisedUsers', value: '403987575024393018' },
    { name: 'AuthorisedUsers', value: '403998829365566266' },
    { name: 'AuthorisedUsers', value: '403999580079135546' },
    { name: 'Account_FoundationDate', value: '2021-01-01T00:00:00Z' },
    { name: 'Corp', value: '291503396865703939' },
    { name: 'Account_CountryText', value: 'Philippines' },
    { name: 'Account_MerchantNotifyNewProductHolding', value: '1' },
    { name: 'Account_hasHealthCover', value: 'No' },
    { name: 'StatusTier', value: '366616251323121921' },
    { name: 'Account_MerchantNotifyConsumerUpdate', value: '1' },
]

// test('', async () => {
//     const csv = fs.readFileSync('trans.csv').toString()
//     const rows = csv.split('\r\n')
//     const errors = [] as any[]

//     for (let i = 1; i < rows.length; i++) {
//         console.log(`row ${i}/${rows.length}`)
//         const r = rows[i]
//         const [resource, key,, message] = r.split(',')
//         const newL = await CrmClient.raw({ method: 'POST', path: 'api/system/locale/resource', data: { lang: 'es', resource, key, message } })
//         if (!newL?.response?.translationID) errors.push([i, resource, key, JSON.stringify(newL)])
//     }

//     if (errors.length) console.table(errors)
// })

test('', async () => {
    const r = await CrmClient.getRecords('383815622561110842', { query: `Status = false AND EndDate < '${new Date(Date.now() - (1000 * 60 * 60 * 24 * 30 * 2)).toISOString()}'` })
    console.log(r)
})
